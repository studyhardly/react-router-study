// 1.导入useNavigate
import { useNavigate } from "react-router-dom"

export default function Login(){
    // 2.执行useNavigate得到一个跳转函数
    const navigate=useNavigate()

    const goto=()=>{ 
        // 3.调用跳转函数传入目标路径
        navigate('/about')
    }
    return (
        <div>登录
            <button onClick={goto}>跳转到关于</button>
            <button onClick={()=>navigate('/about?id=0001')}>跳转带参数SearchParams</button>
            <button onClick={()=>navigate('/content/0001')}>跳转带参数params</button>
        </div>
    )
}