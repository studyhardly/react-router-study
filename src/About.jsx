import { useSearchParams } from "react-router-dom";

export default function About(){
    let [params]=useSearchParams()
    console.log(params,78)
    //params是一个对象,对象里有一个get方法,用来获取对应的参数,把参数名称作为get方法的实参传入进来
    let id=params.get('id')
    return (
        <div>关于页面id:{id}</div>
    )
}