import { BrowserRouter, Routes, Route, Link, useRoutes } from 'react-router-dom'
import Login from './Login'
import About from './About'
import Content from './Content'
import Layout from './Layout'
import Article from './Article'
import Board from './Board'
const Home = () => (<div>首页</div>)
const NotFound = () => (<div>没找到页面</div>)
// 1. 准备一个路由数组 数组中定义所有的路由对应关系
const routesList = [
  {
    path: '/',
    element: <Home />
  },
  {
    path: '/about',
    element: <About />
  },
  {
    path: '/login',
    element: <Login />
  },
  {
    path: '/content/:id',
    element: <Content />
  },
  {
    path: '/layout',
    element: <Layout />,
    children: [
      {

        index: true, // index设置为true 变成默认的二级路由
        element: <Board />,
      },
      {
        path: 'article',
        element: <Article />,
      }
    ]
  },
  // 增加n个路由对应关系
  {
    path: '*',
    element: <NotFound />,
  },
]

// 2. 使用useRoutes方法传入routesList生成Routes组件
function WrapperRoutes() {
  let element = useRoutes(routesList)
  return element
}
function App() {
  return (
    <div className="App">
      {/* 声明当前要使用一个非hash模式的路由 */}
      <BrowserRouter>
        {/* 指定跳转的组件,to用来匹配路由地址 */}
        <Link to='/'>首页</Link>
        <Link to='/about?id=12'>关于</Link>
        <Link to='/content/12'>内容</Link>
        {/* 路由的出口,路由对应的组件会在这里渲染 */}
        <WrapperRoutes></WrapperRoutes>
      </BrowserRouter>
    </div>
  )
}

export default App
