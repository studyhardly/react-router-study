import { Outlet, Link } from 'react-router-dom'

const Layout = () => {
    return (
        <div>
            { /* 默认二级不再具有自己的路径  */}
            <Link to="/layout">board</Link>
            <Link to="/layout/article">article</Link>
            { /* 二级路由出口 */}
            <Outlet />
        </div>
    )
}
export default Layout